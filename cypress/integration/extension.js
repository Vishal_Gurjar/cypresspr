//reference types="Cypress"
describe('My First Test Suite', () => {
    it('requres url works', () => {
        cy.visit('https://reqres.in/')
    });

    it('cypress url works', () => {
        cy.visit('https://www.cypress.io/')
    });

    it('other url',function (){
        cy.visit('https://clinicaltrials.gov/')
    });
})


describe('My First Second Suite', function() {
    it('requres url works',function (){
        //assert:should
        cy.visit('https://rahulshettyacademy.com/seleniumPractise/#/')
        cy.get('.search-keyword').type('br')
        cy.get('.product:visible').should('have.length',2)
        cy.get('.products').find('.product').should('have.length',2)
        cy.get('.products').find('.product').each((ele,index,list)=>{
            cy.log(ele.text)
            if(ele.text().includes("Brinjal"))
            cy.wait(2000)
            cy.wrap(ele).contains("ADD TO CART").click()
        })
        cy.get(".brand.greenLogo").invoke("text").then((value)=>{
            cy.log(value)
               
        })
    });
})      
        
     
//access product dynamically
describe('My First Second Suite', function() {     
    //parent-chill chaining
    it('ADD TO CART VEG INDEX NOm.',function (){
        cy.visit('https://rahulshettyacademy.com/seleniumPractise/#/')
        cy.get('.products').find('.product').each((ele,index,list)=>{
            // cy.log(ele.text())
            if(ele.text().includes("Carrot"))``
            cy.wrap(ele).find("[type='button']").click()
        })
    });    
})

describe("This is our test suit",()=>{
    it('This is our first test case',()=>{
        cy.viewport(1280,720)
        cy.visit('https://rahulshettyacademy.com/AutomationPractice/')
        cy.wait(2000)
        cy.get('#dropdown-class-example').select('Option2').should('have.value','option2')
        cy.get('.inputs.ui-autocomplete-input').type('ame').get('.ui-menu-item-wrapper').each((ele, index)=>{
            // cy.log(ele.text())
            if(ele.text()==='American Samoa')
            cy.wrap(ele).click() 
        })
        // cy.get("#checkBoxOption1").check().should('be.checked').and('have.value','option1') 
        cy.get("[type='checkbox']").check().should('be.checked').and('have.value','option1') 

        // cy.get("#checkBoxOption1").check().should('be.checked').and('have.a.property','option1')
        cy.get("#checkBoxOption2").uncheck().should('not.be.checked')
        cy.get("[value='radio1']").check()
    })
    it('this is doggle test',()=>{
        cy.visit('https://rahulshettyacademy.com/AutomationPractice/')
        cy.get('tr td:nth-child(2)').each((ele,index)=>{
            const txt = ele.text()
            if (txt.includes('Resume'))
            // cy.log('this is index',{index},'This is element',{txt}) 
            cy.get('tr td:nth-child(2)').eq(index).next().then((value)=>{
                cy.log(value.text())
                expect(value.text()).to.equal('0')
            })
        })
    })
    it('Basic commond of cypress',()=>{
        cy.visit('https://rahulshettyacademy.com/').wait(2000)
        cy.get('.nav-outer >.main-menu> .navbar-collapse>.navigation>:nth-child(6)>a').click().wait(2000)
        cy.go("back").wait(2000)     //back or (-1)
        cy.go('forward')             //forward or (+1)
        cy.go(-1).wait(2000)
        cy.go(+2).wait(2000)
    }) 

    it("interect with hidden and visible element",()=>{
        cy.visit('https://rahulshettyacademy.com/AutomationPractice/')
        cy.get('#displayed-text').should('be.visible')
        cy.get('#hide-textbox').click()
        cy.get('#displayed-text').should('not.be.visible')
        cy.get('#show-textbox').click()
        cy.get('#displayed-text').should('be.visible')
    })

    it('alert button',()=>{
        cy.visit('https://rahulshettyacademy.com/AutomationPractice/')
        cy.get('#alertbtn').click()     
        cy.on('window:alert',(popup)=>{
            expect(popup).to.be.equal('Hello , share this practice page and share your knowledge')
        })
    })

    it('second example of alert with OK',()=>{
        cy.visit('http://www.webdriveruniversity.com/Popup-Alerts/index.html')
        cy.get('#button4').click()
        cy.on('window:confirm',(cfn)=>{
            return true
        })
        cy.get('#confirm-alert-text').should('have.text','You pressed OK!')
    })

    it('second example of alert with Cancel',()=>{
        cy.visit('http://www.webdriveruniversity.com/Popup-Alerts/index.html')
        cy.get('#button4').click()
        cy.on('window:confirm',(cfn)=>{
            return false
        })
        cy.get('#confirm-alert-text').should('have.text','You pressed Cancel!')
    })
    it.only('Handle touch and mouse event',()=>{
        cy.visit('https://rahulshettyacademy.com/AutomationPractice/')
        cy.get('#mousehover').should('.mouse-hover-content')    
        cy.on('window:alert',(popup)=>{
            expect(popup).to.be.equal('Hello , share this practice page and share your knowledge')
        })
    })
})
