describe('API Series', () => {
  it('Intercepting API', () => {
      // cy.intercept('https://reqres.in/')

      cy.intercept('GET','**/AutomationPractice/').as('userInfo')
      cy.visit('https://rahulshettyacademy.com/AutomationPractice/')
      cy.wait('@userInfo').then((res)=>{
        cy.log(res)

      })
    
  });
  it('test api with simple intercept',()=>{
    cy.visit('https://jsonplaceholder.typicode.com/')

    cy.intercept({
      path:'/posts'
    }).as('posts')

    cy.get("table:nth-of-type(1) a[href='/posts']").click()
    cy.wait('@posts').then(inter => {
      cy.log(JSON.stringify(inter))
      console.log(JSON.stringify(inter)) 
    })
  })


  it.only('mocking with intercept test',()=>{
    cy.visit('https://jsonplaceholder.typicode.com/')
    cy.intercept('GET','/posts',{totalpost:5,name:'vishal'}).as('posts')
    cy.get("table:nth-of-type(1) a[href='/posts']").click()
    cy.wait('@posts')
  })

})


